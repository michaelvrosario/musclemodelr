#' @title Calculates velocity effects in Hill-type muscle model
#'
#' @description \code{Fvelocity} calculates the effect of velocity on muscle
#'   force generation. This function returns a number from 0 to 1 and represents
#'   the fraction of maximum muscle force that results from a given velocity of
#'   muscle shortening or lengthing.
#'
#' @param Vmus Instantaneous velocity of the muscle in ML/s
#' @param Vmax The maximum velocity of the muscle in ML/s
#' @param plot Boolean indicating whether to generate a plot. Most useful
#'   when Vmus is a vector of values
#' @param add Boolean indicating whether to add data point on current plot.
#'
#' @return A numeric value (or values if Vmus is a vector) ranging from 0 to 1
#'   indicating the fraction of maximum muscle force generated as a result of
#'   velocity effects. This value is usually multiplied by Fmax of the muscle and
#'   by the output of \code{Flength} and/or \code{activation}.
#'
#' @examples
#' Fvelocity(Vmus=2, Vmax=11)
#'
#' Fvelocity(Vmus=seq(from=0, to=11, length.out=1000), plot=T)
#' Fvelocity(Vmus=2, Vmax=11, add=T)
#'
#' @export Fvelocity
Fvelocity <- function(Vmus=seq(from=0, to=11, length.out=1000),
                      Vmax=124.1/11.2, plot=F, add=F){

  scaledForce=NULL

  for(i in 1:length(Vmus)){
    if(Vmus[i] >=0){
      scaledForce <- c(
        scaledForce,
        ((1 - (Vmus[i] / Vmax)) / (1 + (Vmus[i] / (Vmax / 4)))))
    }

    else if(Vmus[i]<0)
      scaledForce <- c(scaledForce, (1.8 - ( (0.8*(1 + Vmus[i] / Vmax )) /
                                               (1 - 7.56 * Vmus[i] /
                                                  (0.25 * Vmax)) )))
  }
  if(plot)
    plot(x=Vmus, y=scaledForce, type="l", lwd=3,
         ylab="Muscle force: velocity effects",
         xlab="Muscle velocity (ML/s)")

  if(add)
    points(x=Vmus, y=scaledForce, pch=19, cex=3, col="green")

  return(scaledForce)
}

#' @title Calculates length effects in Hill-type muscle model
#'
#' @description \code{Flength} calculates the effect of muscle length on
#'   force generation. This function returns a number from 0 to 1,
#'   representing the fraction of maximum muscle force that results from a
#'   given velocity of muscle length.
#'
#' @param L length of the muscle in MLs.
#' @param a parameter the determines "roundness" of L-T curve.
#' (Azizi and Roberts, 2010)
#'  @param b parameter the determines "skewness" of L-T curve.
#' (Azizi and Roberts, 2010)
#'  @param s parameter the determines "width" of L-T curve.
#' (Azizi and Roberts, 2010)
#' @param plot Boolean indicating whether to generate a plot. Most useful
#'   when L is a vector of values
#' @param add Boolean indicating whether to add data point on current plot.
#'
#' @return A numeric value (or values if L is a vector) ranging from 0 to 1
#'   indicating the fraction of maximum muscle force generated as a result of
#'   length effects. This value is usually multiplied by Fmax of the muscle and
#'   by the output of \code{Fvelocity} and/or \code{Factivation}.
#'
#' @examples
#' Flength(L=0.85)
#'
#' Flength(L=seq(from=0.6, to=1.6, length.out=1000), plot=T)
#' Flength(L=0.85, add=T)
#'
#' @export Flength
Flength <- function(L=seq(from=0.6, to=1.4, length.out=1000), a=2.08, b=-2.89,
                    s=-.75, plot=F, add=F){

  scaledForce <- exp(-abs( (L^b-1)/s)^a)

  if(plot)
    plot(x=L, y=scaledForce, type="l", lwd=3,
         ylab="muscle Force:length effects", xlab="Muscle length (MLs)")

  if(add)
    points(x=L,y=scaledForce, pch=19, cex=3, col="green")

  return(scaledForce)
}





#' @title Calculates activation effects in Hill-type muscle model
#'
#' @description \code{Factivation} generates a linear ramp
#'   representing the fraction of active motor units in the muscle. The
#'   activation linearly increase from 0 to 1 within the difined time span
#'   (\code{tmax}). After this time, the activation remains at 1. Instantaneous
#'   maximal activation can be triggered by setting \code{maxAct} to
#'   \code{TRUE}.
#'
#' @param t time in milliseconds
#' @param tmax time at which maximal activation is reached
#' @param maxAct optional boolean value that instantaneously sets activation to 1
#'
#' @return A numeric value (or values if t is a vector) ranging from 0 to 1
#'   indicating the fraction of maximum muscle force generated as a result of
#'   motor recruitment. This value is usually multiplied by Fmax of the muscle and
#'   by the output of \code{Fvelocity} and/or \code{Flength}.
#'
#' @export Factivation
Factivation <- function(t, tmax, maxAct=F){
  k <- 1/tmax
  scaledForce <- k*t

  if(scaledForce>1 | maxAct)
    scaledForce=1

  return(scaledForce)
}




#' @title Calculates activation for next timestep using stimulation input
#'
#' @description \code{getNextActivation} is used to calculate muscle activation
#'   for the next time step in a simulation given level of muscle stimulation.
#'   Options are also included to limit max activation and return a constant
#'   level of activation if required. Activation and stimulation generally span
#'   from 0 (no activation or stimulation) to 1 (max activation or stimulation)
#'   , but these are not explicitly constrained. Formulae used to convert
#'   stimulation to activation is the same as used in openSim.
#'
#' @param lastActivation Level of muscle activation in the previous timestep
#' @param time Time in seconds of the current time step
#' @param dt Interval of each time step
#' @param shift Shift, in seconds, of the start of stimulation in relation to
#'   0 seconds
#' @param tau Time constant used to control muscle activation
#' @param tauDeact Time constant used to control muscle deactivation
#' @param actDynamics Boolean determining whether activation dynamics should be
#'   considered in calculation. If False, min or max activation returned
#'   depending on \code{time} and \code{shift}
#' @param minActivation Minimum activation returned. This value should be set
#'  low, but non-zero. Zero values will break the code because in some
#'  calculations, activation appears in the denominator
#' @param maxActivation Maximum muslce activation
#' @param stimulation stimulation from 0 (no stimulation) to 1 (max
#'   stimulation)
#' @export getNextActivation
getNextActivation <- function(lastActivation, time, dt, shift = 0, tau=0.010,
                              tauDeact=0.040, actDynamics=T,
                              minActivation=0.001, maxActivation=1,
                              stimulation=1,
                              actRamp=F, actDuration=0.100,
                              rampUp=0.010, rampDown=0.010){

  if(actRamp){

    if(time < shift){
      return(minActivation)
    }

    # ramp up
    if(time <= (shift + rampUp)){
      return( (maxActivation/rampUp) * (time-shift))
    }

    # not ramping up
    if(time >= (rampUp + shift)){
      # hold activation
      if(time < (shift + actDuration)){
        return(maxActivation)
      }
      #ramp down
      if(time < (shift + actDuration + rampDown)){
        return( (-maxActivation/rampDown * (time - shift - actDuration) ) + maxActivation)

      } else {
        #passive
        return(minActivation)
      }
    }


  }

  if(!actDynamics){
    ############################################################################ NEED TO WORK IN DEACTIVATION RAMP!!!!
    if(time < shift){
      return(minActivation)
    } else {
      return(maxActivation)
    }
  }

  if(time < shift){
    if(lastActivation == 0){
      return(minActivation)
    } else {
      # if time is less than shift, don't alter activation
      return(lastActivation)
    }


  } else if(time==0){
    # determine first activation
    lastActivation <- 0.001
    tempT <- seq(from=shift, to=0, by=dt)
    for(i in 1:length(tempT)){
      da_dt <- (stimulation - lastActivation) /
        (0.5 + (1.5 * lastActivation)) / tau
      lastActivation <- lastActivation + ( da_dt * dt)
    }
    return(lastActivation)

    } else {
      if(stimulation > lastActivation){
        da_dt <- (stimulation - lastActivation) /
          (0.5 + (1.5 * lastActivation)) / tau
      } else {
        da_dt <- (stimulation - lastActivation) *
          (0.5 + (1.5 * lastActivation)) / tauDeact
      }
      return( lastActivation + ( da_dt * dt) )
  }
}


#' @title Plot activation dynamics
#'
#' @description Given a vector of stimulation inputs, \code{PlotActivation}
#'   will generate a plot of the stimulation input and coresponding muscle
#'   activation. Currently, this function relies on default values provided
#'   to \code{getNextActivation}.
#'
#' @param stimulusInput Vector of inputs ranging from 0 (no stimulation) to
#'     1 (max stimulation)
#' @param dt The timestep of \code{stimulusInput} in seconds
#' @param startingActivation The initial level of muscle activation at time 0.
#'
#' @examples
#' PlotActivation(stimulusInput = c(rep(0,100), rep(1,100)), dt = 1/1000,
#'   startingActivation = 0)
#'
#' PlotActivation(stimulusInput = c(rep(0,100), rep(1,100)), dt = 1/1000,
#'   startingActivation = 1)
#'
#' PlotActivation(stimulusInput = rep(0,200), dt = 1/1000,
#'   startingActivation = 1)
#' @export PlotActivation
PlotActivation <- function(stimulusInput, dt, startingActivation){

  # should always start stimulation at
  time <- seq(from = 0, length.out = length(stimulusInput), by=dt)
  activation <- rep(startingActivation, length(time))

  for(i in 2:length(activation)){
    activation[i] <- getNextActivation(lastActivation = activation[i - 1],
                                       time = time[i], dt = dt,
                                       stimulation = stimulusInput[i])
  }

  plot(x=time, y=stimulusInput, col="purple", type="l", ylim=c(0,1), lwd=3)
  points(x=time, y=activation, col="red")

}







