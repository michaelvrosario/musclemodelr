#' @title Simulate MTU lengthening using PSO
#'
#' @description This function is the main function used to simulate MTU
#'   lengthening for a given combination of muscle activation and MTU
#'   lengthening.
#'
#' @param inputList A list containing the inputs to the MTU
#'   simulation. The list contains two dataframes: \code{constants} and
#'   \code{vectors}. The \code{constants} dataframe is a single-rowed
#'   dataframe containing values necessary for the simulation to run.
#'   The \code{vectors} dataframe is multi-rowed and contains time as
#'   well as muscle activation and MTU length over time. See section
#'   below for full description of necessary inputs to \code{inputList}.

#' @param maxit The maximum number of interations that the Particle Swarm
#'   Optimization will take for convergence before accepting the solution and
#'   moving on to the next timestep. Increase this number if simulation fails
#'   to converge.
#'
#' @param tol The maximum error that is considered for convergence. Increasing
#'   the tolerance may improve speed in exchange for accuracy.
#'
#' @return A list that includes the results of the simulation and the
#'   \code{inputList}. The \code{results} dataframe contains time, x, v, F, w,
#'   and p for the MTU, spring, and muscle. The results also include error in
#'   force of the spring vs. force in the muscle for the numerical integration
#'   (should be close to 0), as well as convergence (0 = convergence). Note
#'   that all values are listed units of seconds, mm, N, J/kg, and W/kg. X
#'   values are relative to length at t=0. Additional calculations are
#'   necessary to convert results into "true" muscle units.
#'
#' @section Structure of \code{inputList$constants}:
#'
#'   useFV: boolean deterimining whether to use force-velocity effects
#'
#'   useLT: boolean deterimining whether to use length-tension effects
#'
#'   k: spring constant; can be Inf (N/mm)
#'   
#'   eta: constant representing viscosity ((N*s)/m)
#'
#'   dt: resolution of timestep (s)
#'
#'   Fmax: maximum isometric force of the muscle (N)
#'
#'   v_mus_max: maximum muscle velocity (L0/s)
#'
#'   L0_mus: resting length of the muscle; length at Fmax (mm)
#'
#'   x_mus_0: starting length of the muscle at t=0 (mm)
#'
#'   m_mus: mass of the muscle used to calculate mass-specific power (kg)
#'
#'   lengthenRate: rate at which MTU is lengthened; negative is
#'   lengthening (mm/s)
#'
#'   lengthenDuration: duration of active lengthening (s)
#'
#'   type: character string indicating the kind of simulation. Options include
#'  "MTU_ramp"
#'
#' @section Structure of \code{inputList$vectors}:
#'
#'   time: time of the simulation in seconds
#'
#'   activation: activation of the muscle. 0 indicates no activation, 1
#'   indicates maximal activation.
#'
#'   x_MTU: length change of the MTU in mm such that 0 indicates MTU length
#'   prior to simulation. Negative values indicate lengthening.
#'
#' @export SimulateMTUlengthening_PSO
SimulateMTUlengthening_PSO <- function(inputList, maxit=5000,
                                       tol=10^-10){

  useFV                <- inputList$constants$useFV
  useLT                <- inputList$constants$useLT
  k                    <- inputList$constants$k
  eta                  <- inputList$constants$eta
  dt                   <- inputList$constants$dt
  Fmax                 <- inputList$constants$Fmax
  v_mus_max            <- inputList$constants$v_mus_max
  l0_mus               <- inputList$constants$l0_mus
  x_mus_0              <- inputList$constants$x_mus_0 # in mm
  m_mus                <- inputList$constants$m_mus # in kg for power calcs

  lengthenRate         <- inputList$constants$lengthenRate
  lengthenDuration     <- inputList$constants$lengthenDuration
  t_maxAct             <- inputList$constants$t_maxAct

  time                 <- inputList$vectors$time
  activation           <- inputList$vectors$activation
  x_MTU                <- inputList$vectors$x_MTU


  # LENGTHENING SIMULATION
  cat("simulating MTU response to length and activation changes\n\n")

  n <- length(time)

  v_MTU <- rep(0, n)
  for(i in 2:n){
    v_MTU[i] <- (x_MTU[i] - x_MTU[i-1]) / time[2]
  }

  F_MTU <- rep(0, n)
  w_MTU <- rep(0, n)
  p_MTU <- rep(0, n)

  # NOTE, all values of x are in mm
  # need to convert to L0 for muscle functions

  # for total simulation
  x_spr         <- rep(0, n)
  v_spr         <- rep(0, n)
  F_spr         <- rep(0, n)
  w_spr         <- rep(0, n)
  p_spr         <- rep(0, n)

  x_mus         <- rep(0, n)
  v_mus         <- rep(0, n)
  F_mus         <- rep(0, n)
  w_mus         <- rep(0, n)
  p_mus         <- rep(0, n)

  errorTotal    <- rep(0, n)
  convergence   <- rep(0, n)

  # parameters that remain constant for the duration of ENTIRE simulation
  fixedParams <- data.frame(
    dt,
    Fmax,
    v_mus_max,
    l0_mus,
    x_mus_0,
    k,
    eta,
    useLT,
    useFV
  )

  # progress bar
  pbar <- txtProgressBar(min = 1, max = n, initial = 1, style = 3)

  # add parameters that vary each timestep
  for (i in 2:n){

    # create stimulation from dynamic parameters
    fixedParams$activation <- activation[i]
    fixedParams$x_mus_previous <- x_mus[i-1]
    fixedParams$x_spr_previous <- x_spr[i-1]
    fixedParams$x_MTU <- x_MTU[i]

    ### finite spring stiffness
    if(k != Inf){
      pso_results <- pso::psoptim(
        par = rep(NA, 1),
        fn = getError_lengthen,
        fixedParams = fixedParams,
        lower = -v_mus_max * 1.5 * l0_mus,
        upper = v_mus_max * 1.5 * l0_mus,
        control = list(maxit = maxit, abstol = tol))

      v_mus[i] <- pso_results$par[1] # mm/s
      x_mus[i] <- x_mus[i - 1] + (v_mus[i] * dt)
      errorTotal[i] <- pso_results$value
      convergence[i] <- pso_results$convergence

      # spring outputs
      #v_spr[i] <- v_MTU[i] - v_mus[i]
      #x_spr[i] <- x_spr[i - 1] + (v_spr[i] * dt)
      x_spr[i] <- x_MTU[i] - x_mus[i]
      v_spr[i] <- (x_spr[i] - x_spr[i-1]) / dt
      F_spr[i] <- k * x_spr[i] + (v_spr[i] * eta)

      dx_spr <- x_spr[i] - x_spr[i-1]
      w_spr[i] <- -mean(c(F_spr[i], F_spr[i-1])) * dx_spr

    } else { # infinite spring stiffness
      v_mus[i] <- v_MTU[i]
      errorTotal[i] <- 0
      convergence[i] <- 0

    }



    # muscle outputs
    x_mus[i] <- x_mus[i - 1] + (v_mus[i] * dt)

    FV <- 1
    LT <- 1

    if(useLT){
      FL <- Flength(L = (x_mus_0 + x_mus[i])/l0_mus, plot = F)
    }

    if(useFV){
      FV <- Fvelocity(Vmus = v_mus[i] / l0_mus, Vmax = v_mus_max)
    }

    F_mus[i] <- -Fmax * activation[i] * FV * LT
    dx_mus <- x_mus[i] - x_mus[i - 1]
    w_mus[i] <- -mean(c(F_mus[i], F_mus[i - 1])) * dx_mus


    # MTU outputs
    F_MTU[i] <- F_mus[i]
    dx_MTU <- x_MTU[i] - x_MTU[i - 1]
    w_MTU[i] <- -mean(c(F_MTU[i], F_MTU[i - 1])) * dx_MTU


    setTxtProgressBar(pb = pbar, value = i)
  }
  close(pbar)

  # calculate work in terms of J / kg of muscle
  w_spr <- cumsum(w_spr) / 1000 / m_mus
  w_mus <- cumsum(w_mus) / 1000 / m_mus
  w_MTU <- cumsum(w_MTU) / 1000 / m_mus

  for(i in 2:length(v_spr)){
    p_spr[i] <- (w_spr[i] - w_spr[i - 1]) / dt
    p_mus[i] <- (w_mus[i] - w_mus[i - 1]) / dt
    p_MTU[i] <- (w_MTU[i] - w_MTU[i - 1]) / dt
  }


  output <- list()

  output$results <-
    data.frame(
      time,
      x_MTU,
      v_MTU,
      F_MTU,
      w_MTU,
      p_MTU,

      x_spr,
      v_spr,
      F_spr,
      w_spr,
      p_spr,

      x_mus,
      v_mus,
      F_mus,
      w_mus,
      p_mus,

      activation,
      errorTotal,
      convergence
    )

  output$results <- as.list(output$results)

  output$input <- inputList
  return(output)
}




#' @title Cost function for numerical optimization of MTU lengthening
#'
#' @description In the cost function, velocity of the muscle is assumed. Given
#'   parameters from the initial conditions and previous results, the resulting
#'   force of the muscle and spring are calculated. The error is calculated as
#'   the absolute value of the difference from the calculated muscle and spring
#'   force.
#'
#' @param beta The guess for muscle velocity (mm/s)
#' @param fixedParams Dataframe of parameters that remain constant during the
#'   minmization process. These values are imported from \code{inputParams} or
#'   calculated just prior to use at the current time step.
#' @export getError_lengthen
getError_lengthen <- function(beta, fixedParams){

  dt              <- fixedParams$dt
  Fmax            <- fixedParams$Fmax
  v_mus_max       <- fixedParams$v_mus_max
  l0_mus          <- fixedParams$l0_mus
  x_mus_0         <- fixedParams$x_mus_0
  x_spring        <- fixedParams$x_spring
  x_MTU           <- fixedParams$x_MTU
  activation      <- fixedParams$activation
  k               <- fixedParams$k
  eta             <- fixedParams$eta
  x_mus_previous  <- fixedParams$x_mus_previous
  x_spr_previous  <- fixedParams$x_spr_previous
  useLT           <- fixedParams$useLT
  useFV           <- fixedParams$useFV

  v_mus <- beta[1] # mm/s
  x_mus <- x_mus_previous + (v_mus * dt) # negative x for lengthening
  x_spr <- x_MTU - x_mus # negative x_spr for lengthening
  v_spr <- ( x_spr - x_spr_previous)/ dt
  F_spr <- k * x_spr + (v_spr * eta)

  # check to see if LT effects are used
  FL <- 1
  if(useLT){ # no LT effects
    FL <- Flength(L = (x_mus_0 + x_mus)/l0_mus, plot = F)
  }

  FV <- 1
  if(useFV){
    FV <- Fvelocity(Vmus = v_mus / l0_mus, Vmax = v_mus_max)
  }

  F_mus <- -Fmax * activation * FV * FL # negative force stretches spring

  return(abs(F_spr - F_mus))
}

#' @title Create an inputList for muscle simulation.
#'
#' @description This functions as a shortcut for creating an input list for
#'   specific types of simulations. Currently, this function supports MTU ramp
#'   experiments.
#'
#' @param constants single-rowed dataframe of constants required for
#'   simulation. See section below for details on inputs of the
#'   \code{constants} dataframe.
#'
#' @param type Designates the type of input to create. "MTU_ramp" generates a
#'   linear ramp in MTU length (negative indicates lengthening) with maximal
#'   muscle unit activation.
#'
#' @section Structure of \code{constants} dataframe:
#'
#'   useFV: boolean deterimining whether to use force-velocity effects
#'
#'   useLT: boolean deterimining whether to use length-tension effects
#'
#'   k: spring constant; can be Inf (N/mm)
#'   
#'   eta: constant representing viscosity ((N*s)/m)
#'
#'   dt: resolution of timestep (s)
#'
#'   Fmax: maximum isometric force of the muscle (N)
#'
#'   v_mus_max: maximum muscle velocity (L0/s)
#'
#'   L0_mus: resting length of the muscle; length at Fmax (mm)
#'
#'   x_mus_0: starting length of the muscle at t=0 (mm)
#'
#'   m_mus: mass of the muscle used to calculate mass-specific power (kg)
#'
#'   lengthenRate: rate at which MTU is lengthened; negative is
#'   lengthening (mm/s)
#'
#'   lengthenDuration: duration of active lengthening (s)
#'
#'
#' @export makeInputList
makeInputList <- function(constants){

  inputList <- list()
  inputList$constants <- as.list(constants)
  type <- inputList$constants$type

  if(type=="MTU_ramp"){
    time = seq(from=0, by=constants$dt, to=constants$lengthenDuration)
    activation = rep(1, length(time))
    x_MTU = seq(from=0, by=constants$lengthenRate * constants$dt,
                to=constants$lengthenRate * constants$lengthenDuration)

    vectors <- list(time=time, activation=activation, x_MTU=x_MTU)

    inputList$vectors <- vectors
  } else {
    cat("\n No method matches selected inputList type.\n\n")
  }

  return(inputList)
}

#' @title Wrapper function for running multiple simulations
#'
#' @description This function allows batch simulation of MTU dynamics.
#'   Simulations are exported to *.JSON file format after each simulation,
#'   so that runs can be cancelled without losing previously simulated data.
#'
#' @param inputs dataframe in which each row represents a single simulation.
#'   Each row will be passed to \code{makeInputList} prior to each run and
#'   must include all parameters to ensure inputLists are constructed properly.
#' @param file Character string indicating the filename of the exported data.
#'   Make sure to include the *.JSON extension. This is also the file that
#'   will be read in if \code{previousResults} is set to \code{TRUE}.
#' @param previousResults Boolean indicating whether or not to import previous
#'   data from *.JSON format.
#'
#' @export RunMultipleSimulations
RunMultipleSimulations <- function(inputs, file, previousResults=F){

  # setup output for first runs
  if((!previousResults)){
    output <- list()
    output$simulations <- list()
    output$simulations[[1]] <- names(inputs)
    i <- 1 # keep track of index of next simulation
  } else {
    # import JSON
    output <- importJSON(file = file)
    i <- length(output$simulations)  # keep track of index of last simulation
  }

  indices <- 1:nrow(inputs)
  toRemove <- NULL
  # check each simulation agaist previous records, skip duplicates
  if(previousResults){
    for(k in indices){
      inputsForSim <- as.character(inputs[k,])
      for(l in 2:length(output[[1]])){
        if(isTRUE(all.equal.character(inputsForSim, output[[1]][[l]]))){
          print(inputsForSim)
          toRemove <- c(toRemove, k)
          cat("skipping simulation\n")
          break
        }
      }

    }

    indices <- indices[-toRemove]
  }

  for(j in 1:length(indices)){


    # append metadata to output[[1]]
    output$simulations[[i+j]] <- as.character(inputs[indices[j],])

    # run simulation
    inputList <- makeInputList(constants = inputs[indices[j],])
    sim_results <- SimulateMTUlengthening_PSO(inputList = inputList)

    output[[i+j]] <- sim_results

    # export data
    exportJSON(list = output, file = file)
  }
}

#' @title Plots a matrix of time series from muscle simulation data
#'
#' @description Given a list of muscle simulation data, time series of x, v, F,
#'   w, and p are plotted over time. Additionally, data are plotted for the
#'   MTU, muscle, and spring. If exported to PDF, the parameters of each
#'   simulation are appended to the file and color coded to match plots.
#'
#' @param data A list of simulation results. Each element of this list is the
#'   output of \code{SimulateMTUlengthening_PSO}.
#' @param pdf boolean indicating whether or not to plot to pdf. If set to
#'   \code{FALSE}, plot is set to plotting window. Otherwise,
#'   "simulation_results.pdf" will be created in working directory.
#' @param ySymmetry boolean. When set to \code{TRUE}, y axes are scaled such
#'   that they extend to the same extent in both the positive and negative
#'   directions. Otherwise, autoscaling of axes are used.
#' @param colors A vector of colors to indicate different datasets. The
#'   number of colors should be greater than or equal to the number of
#'   simulations passed.
#' @export PlotTimeSeries
PlotTimeSeries <- function(data, pdf=T, ySymmetry = T, colors = palette()){

  if(pdf){
    pdf("lengthening_timeSeries.pdf", height = 8.5, width = 11)
  }

  marDefault <- par("mar")
  mfrowDefault <- par("mfrow")
  # set margins to reduce space between plots
  par(mfrow = c(5,3), mar = c(3,4.5,2,2))


  # get min and max vals for all data
  tmin <- NULL
  tmax <- NULL

  xmin <- NULL
  xmax <- NULL

  vmin <- NULL
  vmax <- NULL

  Fmin <- NULL
  Fmax <- NULL

  wmin <- NULL
  wmax <- NULL

  pmin <- NULL
  pmax <- NULL

  for(i in 2:length(data)){

    time <- data[[i]]$results$time
    tmin <- min(c(tmin, min(time)))
    tmax <- max(c(tmax, max(time)))

    x_MTU <- data[[i]]$results$x_MTU
    x_mus <- data[[i]]$results$x_mus
    x_spr <- data[[i]]$results$x_spr
    xmin <- min(c(xmin, min(x_MTU), min(x_mus), min(x_spr)))
    xmax <- max(c(xmax, max(x_MTU), max(x_mus), max(x_spr)))

    v_MTU <- data[[i]]$results$v_MTU
    v_mus <- data[[i]]$results$v_mus
    v_spr <- data[[i]]$results$v_spr
    vmin <- min(c(vmin, min(v_MTU), min(v_mus), min(v_spr)))
    vmax <- max(c(vmax, max(v_MTU), max(v_mus), max(v_spr)))

    F_MTU <- data[[i]]$results$F_MTU
    F_mus <- data[[i]]$results$F_mus
    F_spr <- data[[i]]$results$F_spr
    Fmin <- min(c(Fmin, min(F_MTU), min(F_mus), min(F_spr)))
    Fmax <- max(c(Fmax, max(F_MTU), max(F_mus), max(F_spr)))

    w_MTU <- data[[i]]$results$w_MTU
    w_mus <- data[[i]]$results$w_mus
    w_spr <- data[[i]]$results$w_spr
    wmin <- min(c(wmin, min(w_MTU), min(w_mus), min(w_spr)))
    wmax <- max(c(wmax, max(w_MTU), max(w_mus), max(w_spr)))

    p_MTU <- data[[i]]$results$p_MTU
    p_mus <- data[[i]]$results$p_mus
    p_spr <- data[[i]]$results$p_spr
    pmin <- min(c(pmin, min(p_MTU), min(p_mus), min(p_spr)))
    pmax <- max(c(pmax, max(p_MTU), max(p_mus), max(p_spr)))
  }

  datatype <- c(
    "x_MTU", "x_mus", "x_spr",
    "v_MTU", "v_mus", "v_spr",
    "F_MTU", "F_mus", "F_spr",
    "w_MTU", "w_mus", "w_spr",
    "p_MTU", "p_mus", "p_spr"
  )

  # check for y axis symmetry
  if(ySymmetry){

    xmin <- -max(c(abs(xmin), abs(xmax)))
    xmax <- abs(xmin)

    vmin <- -max(c(abs(vmin), abs(vmax)))
    vmax <- abs(vmin)

    Fmin <- -max(c(abs(Fmin), abs(Fmax)))
    Fmax <- abs(Fmin)

    wmin <- -max(c(abs(wmin), abs(wmax)))
    wmax <- abs(wmin)

    pmin <- -max(c(abs(pmin), abs(pmax)))
    pmax <- abs(pmin)
  }

  limits <- c(
    xmin, xmax,
    vmin, vmax,
    Fmin, Fmax,
    wmin, wmax,
    pmin, pmax)

  ylabs <- c(
    "x_MTU (mm)", "x_mus (mm)", "x_spr (mm)",
    "v_MTU (mm/s)", "v_mus (mm/s)", "v_spr (mm/s)",
    "F_MTU (N)", "F_mus (N)", "F_spr (N)",
    "w_MTU (J/kg)", "w_mus (J/kg)", "w_spr (J/kg)",
    "p_MTU (W/kg)", "p_mus (W/kg)", "p_spr (W/kg)"
  )

  # begin plotting panel by panel
  for(i in 1:(5)){

    # MTU
    plot(x = c(tmin, tmax), y=c(limits[2*i-1], limits[2*i]), type="n",
         xlab="time (s)", ylab=ylabs[3*i-2])
    abline(h=0, lty=3)

    for(j in 1:length(data)){
      lines(x=data[[j]]$results$time,
            y=data[[j]]$results[[datatype[3*i - 2]]], col=colors[j])
    }

    # muscle
    plot(x = c(tmin, tmax), y=c(limits[2*i-1], limits[2*i]), type="n",
         xlab="time (s)", ylab=ylabs[3*i-1])
    abline(h=0, lty=3)

    for(j in 1:length(data)){
      lines(x=data[[j]]$results$time,
            y=data[[j]]$results[[datatype[3*i - 1]]], col=colors[j])
    }

    # spring
    plot(x = c(tmin, tmax), y=c(limits[2*i-1], limits[2*i]), type="n",
         xlab="time (s)", ylab=ylabs[3*i])
    abline(h=0, lty=3)

    for(j in 1:length(data)){
      lines(x=data[[j]]$results$time,
            y=data[[j]]$results[[datatype[3*i]]], col=colors[j])
    }
  }

  # setting up empty plot to print out parameters
  par(mfrow=c(1,1))
  if(pdf){
    for(i in 2:length(data)){
      plot(x=0, y=0, type="n")

      # add parameters to dataset
      y.text <- seq(from=1, to=-1,
                    length.out=length(data[[i]]$input$constants))
      x.text <- rep(0, length(data[[i]]$input$constants))

    for(j in 1:length(data[[i]]$input$constants)){
      text(
        x = x.text[j],
        y=y.text[j],
        labels=paste(names(data[[i]]$input$constants)[j],": ",
                     data[[i]]$input$constants[j], sep=""), col = colors[i])
      }
    }

    dev.off()
  }
  par(mfrow=mfrowDefault, mar=marDefault)
}
