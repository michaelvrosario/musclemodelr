test_that("check that activation dynamics are implemented properly", {

  # no activation dynamics
  activation <- getNextActivation(lastActivation=0, shift=0, time=0, dt=1,
                                  actDynamics=F, maxActivation=1)
  expect_that(activation, equals(1))


  activation <- getNextActivation(lastActivation=0, shift=0.050, time=0, dt=1,
                                  actDynamics=F, maxActivation=1)
  expect_that(activation, equals(0.001))


  activation <- getNextActivation(lastActivation=0, shift=-0.050, time=0, dt=1,
                                  actDynamics=F, maxActivation=1)
  expect_that(activation, equals(1))


  activation <- getNextActivation(lastActivation=0, shift=0, time=0, dt=1,
                                  actDynamics=F, maxActivation=0.5)
  expect_that(activation, equals(0.5))




  # first order activation
  activation <- getNextActivation(lastActivation=0, shift=0, time=0, dt=1/1000,
                                  actDynamics=T, maxActivation=1)
  expect_equal(activation, 0.2002024)


  activation <- getNextActivation(lastActivation=0, shift=0, time=0.5,
                                  dt=1/1000, actDynamics=T, maxActivation=1)
  expect_equal(activation, 0.2)


  activation <- getNextActivation(lastActivation=0, shift=0.050, time=0,
                                  dt=1/1000, actDynamics=T, maxActivation=1)
  expect_equal(activation, 0.001)


  activation <- getNextActivation(lastActivation=0, shift=-0.050, time=0,
                                  dt=1/1000, actDynamics=T, maxActivation=1)
  expect_equal(round(activation, digits=4), 0.9667)


  # first order deactivation
  activation <- getNextActivation(lastActivation=1, shift=0, time=1, dt=1/1000,
                                  actDynamics=T, maxActivation=1, stimulation=0)
  expect_equal(round(activation, digits=4), 0.95)

  activation <- getNextActivation(lastActivation=0.5, shift=2, time=1, dt=1/1000,
                                  actDynamics=T, maxActivation=1, stimulation=0)
  expect_equal(round(activation, digits=4), 0.5)

})
